-- Creating roles
DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname = '${api_user}') THEN
    CREATE USER ${api_user} WITH ENCRYPTED PASSWORD '${api_password}';
    GRANT ALL ON ALL TABLES IN SCHEMA public TO ${api_user};
  END IF;

  IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname = '${core_user}') THEN
    CREATE USER ${core_user} WITH ENCRYPTED PASSWORD '${core_password}';
    GRANT ALL ON ALL TABLES IN SCHEMA public TO ${core_user};
  END IF;
END
$$;
