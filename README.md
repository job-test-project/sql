# Некоторые решения

## Создание юзеров

Чтобы не создавать руками. В миграциях пользователям выдаются все права, но это лишь для примера.

# Описание

Репозиторий с миграциями для имзенения бд.

## Запуск

1. Скачать [liquibase](https://www.liquibase.org/download) или через homebrew `homebrew install liquibase`

2. Создайте файл `liquibase.properties` по прототипу `liquibase.properties.example`, где:

- `PG_USERNAME` - имя PostgreSQL пользователя
- `PG_PASSWORD` - пароль PostgreSQL пользователя
- `PG_DB_NAME` - название бд PostgreSQL
- `PG_HOST` - адрес, где запущен PostgreSQL
- `PG_PORT` - порт, на котором запущен PostgreSQL
- `PG_JDBC_DRIVER_RELATIVE_PATH` - путь, относительный текущей папки репозитория, до [JDBC драйвера для PostgreSQL](https://jdbc.postgresql.org/download.html)
- `PG_API_USERNAME` - имя будущего PostgreSQL пользователя, которым будет пользоваться API приложениe
- `PG_API_PASSWORD` - пароль будущего PostgreSQL пользователя, которым будет пользоваться API приложениe
- `PG_CORE_USERNAME` - имя будущего PostgreSQL пользователя, которым будет пользоваться Core приложение
- `PG_CORE_PASSWORD` - пароль будущего PostgreSQL пользователя, которым будет пользоваться Core приложение

3. Выполните `liquibase update`

## Добавление миграций

1. Создайте папку с названием `${текущая_дата}-${о_чем_миграция}`
2. Создайте `changelog.xml`
3. Обязательно обновите тэг у `tagDatabase` и названия обоих `changeSet`
4. Пропишите саму миграция (`rollback` обязателен!)
5. Выполните `liquibase update`
6. Проверьте что роллбэк работает, выполнив `liquibase rollback ${тэг}`
